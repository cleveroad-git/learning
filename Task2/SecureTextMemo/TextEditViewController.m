//
//  TextEditViewController.m
//  SecureTextMemo
//
//  Created by Dmitriy on 01.09.14.
//  Copyright (c) 2014 IDS. All rights reserved.
//

#import "TextEditViewController.h"
#import "Obfuscate.h"
#import "SAMHUDView.h"


@interface TextEditViewController ()<UIAlertViewDelegate>

@property (strong, nonatomic)IBOutlet UITextView *textMemo;
@property(assign,nonatomic)BOOL isEdit;
@property(weak,nonatomic)NSString* info;
@property(assign,nonatomic)CGRect oldFrame;
@property(strong,nonatomic)UIBarButtonItem* doneButton;
@property(strong,nonatomic)NSString* directoryPath;

@end

@implementation TextEditViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated
{
    if (_isEdit)
        [_textMemo becomeFirstResponder];
    else
    {
        self.info = [PasswordSingleton sharedInstanses].fileName;
        _textMemo.text = [[NSString alloc]initWithData:[Obfuscate obfuscate:[NSData dataWithContentsOfFile:[_directoryPath stringByAppendingPathComponent:_info]] withKey:[PasswordSingleton sharedInstanses].password] encoding:NSUTF8StringEncoding];
        

    }
    
}

-(void)viewWillLayoutSubviews
{
    [_textMemo setFrame:_oldFrame];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self registerForKeyboardNotifications];
    _directoryPath =  [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    _doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(saveMemoToFile)];
    _textMemo.delegate = self;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning

{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)textViewDidChange:(UITextView *)textView
{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(saveMemoToFile)];
}

-(void)getInfo:(BOOL)isEdit
{
    self.isEdit = isEdit;
}

-(void)saveMemoToFile
{
    [self closeKeyboard:self];
    if (!_isEdit) {
        NSString* filePath = [_directoryPath stringByAppendingPathComponent:_info];
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:NULL];
    }
    UIAlertView *getPassword = [[UIAlertView alloc] initWithTitle:@"Password!" message:@"Type new password:" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    getPassword.alertViewStyle = UIAlertViewStyleSecureTextInput;
    getPassword.delegate = self;
    [getPassword show];
    

}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([[alertView buttonTitleAtIndex:buttonIndex]isEqual:@"OK"])
    {
        [PasswordSingleton sharedInstanses].password = [NSMutableString stringWithString:[alertView textFieldAtIndex:0].text];
        [[Obfuscate obfuscate:[_textMemo.text dataUsingEncoding:NSUTF8StringEncoding] withKey:[PasswordSingleton sharedInstanses].password]
         writeToFile:[_directoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.txt",                                                                     [_textMemo.text substringToIndex:5]]]atomically:YES];
        SAMHUDView* _myHUD = [[SAMHUDView alloc] initWithTitle:@"Writing file" loading:YES];
        [_myHUD show];
        double delayInSeconds = 1.5;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);

        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [_myHUD completeAndDismissWithTitle:@"All Done!"];
            [self performSegueWithIdentifier:@"unwindToMain" sender:self];

        });
    }

}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{

    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    CGRect keyboardEndFrame;

    NSDictionary* info = [aNotification userInfo];
    
    [[info objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[info objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.1f];
    [UIView setAnimationCurve:animationCurve];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    self.oldFrame = self.textMemo.frame;
    CGRect newFrame = self.textMemo.frame;

    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
    {
        newFrame.size.height-=kbSize.width+4;
    }
    else
        newFrame.size.height-=kbSize.height+4;
    
    [self.textMemo setFrame:newFrame];
    [UIView commitAnimations];

}
- (IBAction)closeKeyboard:(id)sender
{
    [self.textMemo resignFirstResponder];
}

// Called when the UIKeyboardWillHideNotification is sent

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [self.textMemo setFrame:_oldFrame];
    [UIView commitAnimations];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
