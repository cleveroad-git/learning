//
//  PasswordSingleton.h
//  SecureTextMemo
//
//  Created by Dmitriy on 02.09.14.
//  Copyright (c) 2014 IDS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PasswordSingleton : NSObject
+(PasswordSingleton*)sharedInstanses;
@property(nonatomic,strong)NSMutableString* password;
@property(nonatomic,strong)NSMutableString* fileName;
@end
