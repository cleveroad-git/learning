//
//  PasswordSingleton.m
//  SecureTextMemo
//
//  Created by Dmitriy on 02.09.14.
//  Copyright (c) 2014 IDS. All rights reserved.
//

#import "PasswordSingleton.h"

@implementation PasswordSingleton
+(PasswordSingleton*)sharedInstanses
{
    static PasswordSingleton *_sharedInstanse = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (_sharedInstanse == nil) {
            _sharedInstanse = [[PasswordSingleton alloc] init];
        }
    });
    return _sharedInstanse;
}
-(id)init
{
    if(self = [super init])
    {
        _password = [NSMutableString new];
        _fileName = [NSMutableString new];

    }
    return self;
}
@end
