//
//  ViewController.m
//  SecureTextMemo
//
//  Created by Dmitriy on 01.09.14.
//  Copyright (c) 2014 IDS. All rights reserved.
//

#import "ViewController.h"
#import "TextEditViewController.h"
#import "Obfuscate.h"

@interface ViewController ()
@property(strong,nonatomic)NSMutableArray* memoArray;
@property (weak, nonatomic) IBOutlet UITableView *memoTable;

@end

@implementation ViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    _memoArray = [NSMutableArray new];
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
   
	// Do any additional setup after loading the view, typically from a nib.
}


- (IBAction)unwindToThisViewController:(UIStoryboardSegue *)unwindSegue
{
    
}



-(void)viewWillAppear:(BOOL)animated
{
    [self loadData];
    [_memoTable reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   return [_memoArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * const cellName = @"memoField";
    UITableViewCell* myCell = [tableView dequeueReusableCellWithIdentifier:cellName];
    myCell.textLabel.text = [_memoArray objectAtIndex:indexPath.row];
    return myCell;
}


-(void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:YES];
    [_memoTable setEditing:editing animated:YES];
}
//Load list of files in document directory
-(void)loadData
{
    NSString* directoryPath =  [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSArray* filePathes = (NSMutableArray*)[[NSFileManager defaultManager] subpathsOfDirectoryAtPath:directoryPath   error:nil];
    NSPredicate* filter = [NSPredicate predicateWithFormat:@"self ENDSWITH '.txt'"];
    _memoArray = [NSMutableArray arrayWithArray:[filePathes filteredArrayUsingPredicate:filter]];
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle==UITableViewCellEditingStyleDelete)
    {
        NSLog(@"%i", indexPath.row);
        [_memoArray removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                         withRowAnimation:UITableViewRowAnimationFade];
    }
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"addNote"])
    {
        [[segue destinationViewController] getInfo:YES];
    }
    if ([segue.identifier isEqualToString:@"editNote"])
    {
        NSIndexPath* path = [self.memoTable indexPathForSelectedRow];
        [PasswordSingleton sharedInstanses].fileName = [_memoArray objectAtIndex:path.row];
    }
}


@end
