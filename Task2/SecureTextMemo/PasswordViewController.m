//
//  PasswordViewController.m
//  SecureTextMemo
//
//  Created by Dmitriy on 01.09.14.
//  Copyright (c) 2014 IDS. All rights reserved.
//

#import "PasswordViewController.h"
#import "ViewController.h"
@interface PasswordViewController ()
@property (weak, nonatomic) IBOutlet UITextField *passwordLabel;
@property (weak, nonatomic) IBOutlet UIButton *buttonNext;

@end

@implementation PasswordViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)closeKeyboard:(id)sender
{
    [_passwordLabel resignFirstResponder];
    [self passwordEntered:self];
}

- (IBAction)passwordEntered:(id)sender
{
    if (![_passwordLabel.text  isEqual:@""]) {
        [PasswordSingleton sharedInstanses].password =  (NSMutableString*)_passwordLabel.text;
        [self performSegueWithIdentifier:@"test" sender:nil];
    }
    else
    {
    UIAlertView* myAlert = [[UIAlertView alloc]initWithTitle:@"No Password" message:@"Enter password" delegate:self cancelButtonTitle:@"OK!" otherButtonTitles:nil];
    [myAlert show];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
