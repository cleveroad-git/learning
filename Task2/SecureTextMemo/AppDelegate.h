//
//  AppDelegate.h
//  SecureTextMemo
//
//  Created by Dmitriy on 01.09.14.
//  Copyright (c) 2014 IDS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
