//
//  TextEditViewController.h
//  SecureTextMemo
//
//  Created by Dmitriy on 01.09.14.
//  Copyright (c) 2014 IDS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextEditViewController : UIViewController<UITextViewDelegate>
-(void)getInfo:(BOOL)isEdit;
@end
