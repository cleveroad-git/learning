//
//  Obfuscate.m
//  SecureTextMemo
//
//  Created by Dmitriy on 04.09.14.
//  Copyright (c) 2014 IDS. All rights reserved.
//

#import "Obfuscate.h"

@implementation Obfuscate
+ (NSData *)obfuscate:(NSData *)string withKey:(NSString *)key;
{
    // Create data object from the string
   
    NSMutableData *result = [string mutableCopy];
    
    
    // Get pointer to data to obfuscate
    char *dataPtr = (char *) [result mutableBytes];
    
    // Get pointer to key data
    char *keyData = (char *) [[key dataUsingEncoding:NSUTF8StringEncoding] bytes];
    
    // Points to each char in sequence in the key
    char *keyPtr = keyData;
    int keyIndex = 0;
    
    // For each character in data, xor with current value in key
    for (int x = 0; x < [string length]; x++)
    {
        // Replace current character in data with
        // current character xor'd with current key value.
        // Bump each pointer to the next character
        *dataPtr = *dataPtr ^ *keyPtr;
        dataPtr++;
        keyPtr++;
        
        // If at end of key data, reset count and
        // set key pointer back to start of key value
        if (++keyIndex == [key length])
            keyIndex = 0, keyPtr = keyData;
    }
    return result;
}

@end
