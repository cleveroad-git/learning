//
//  Obfuscate.h
//  SecureTextMemo
//
//  Created by Dmitriy on 04.09.14.
//  Copyright (c) 2014 IDS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Obfuscate : NSObject
+ (NSData *)obfuscate:(NSData *)string withKey:(NSString *)key;
@end
